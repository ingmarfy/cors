package cors

import (
	"gitee.com/billcoding/flygo"
	"strings"
)

//Define *cors struct
type cors struct {
	origin       string
	methods      []string
	headers      []string
	extraHeaders []*header
}

type header struct {
	name  string
	value string
}

func Header(name, value string) *header {
	return &header{
		name:  name,
		value: value,
	}
}

func New() *cors {
	return &cors{
		origin:  "*",
		methods: []string{"GET", "POST", "PUT", "DELETE", "PATCH"},
	}
}

func (cs *cors) Origin(origin string) *cors {
	cs.origin = origin
	return cs
}

func (cs *cors) Methods(methods ...string) *cors {
	cs.methods = methods
	return cs
}

func (cs *cors) Headers(headers ...string) *cors {
	cs.headers = headers
	return cs
}

func (cs *cors) ExtraHeaders(extraHeaders ...*header) *cors {
	cs.extraHeaders = extraHeaders
	return cs
}

func (cs *cors) Name() string {
	return "FlygoCORS"
}

func (cs *cors) Type() string {
	return "BEFORE"
}

func (cs *cors) Pattern() string {
	return "/**"
}

func (cs *cors) Process() flygo.FilterHandler {
	return func(c *flygo.FilterContext) {
		c.ResponseHeader.Set("Access-Control-Allow-Origin", cs.origin)
		if cs.methods != nil && len(cs.methods) > 0 {
			c.ResponseHeader.Set("Access-Control-Allow-Methods", strings.Join(cs.methods, ","))
		}
		if cs.headers != nil && len(cs.headers) > 0 {
			c.ResponseHeader.Set("Access-Control-Allow-Headers", strings.Join(cs.headers, ","))
		}
		if cs.extraHeaders != nil && len(cs.extraHeaders) > 0 {
			for _, extraHeader := range cs.extraHeaders {
				c.ResponseHeader.Set(extraHeader.name, extraHeader.value)
			}
		}
	}
}
